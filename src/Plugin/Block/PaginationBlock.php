<?php

namespace Drupal\ajax_pagination_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\ajax_pagination_block\Controller\PaginationController;

/**
 * Search Block
 *
 * @Block(
 *   id = "ajax_pagination_block",
 *   admin_label = @Translation("Ajax Pagination Block"),
 *   category = @Translation("Ajax Pagination Block"),
 * )
 */
class PaginationBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $paginationController = new PaginationController();
    $nodeData = $paginationController->getNodeData();
//    kint($nodeData); die;
    return $nodeData;
  }
}
