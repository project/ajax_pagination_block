<?php

namespace Drupal\ajax_pagination_block\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\AjaxResponse;

/**
 * Class PaginationController.
 *
 * @package Drupal\ajax_pagination_block\Controller
 */
class PaginationController extends ControllerBase {

  public function getNodeData(){
    $q1 = $query = \Drupal::entityQuery('node')
      ->condition('type', 'article');

    $q2 = clone $q1;
    $count = $q1->count()->execute();
    $limit = 5;
    $pages = ceil($count / $limit);
    $page = $this->getRequestParam('page', 1);
    if ($page == 1) {
      $offset = 0;
    }
    else {
      $offset = ($page * $limit) - $limit;
    }
    $q2->range($offset, $limit);
    $node_id_article = $q2->execute();

    $currPage = $page;
    $nextAjaxRes = [];
    $prevAjaxRes = [];
    $p = $page;
    if ($pages - $page >= 4) {
      $p = $page;
    }
    else {
      if ($pages >= 5) {
        $p = $page - (4 - ($pages - $page));
      }
      else {
        $p = $page - (($pages - 1) - ($pages - $page));
      }
    }

    for (; $p < $page + 5 && $p <= $pages; $p++) {
      if ($p <= $page) {
        if ($page != 1) {
          $items['prev'] = [
            '#type' => 'link',
            '#title' => '«',
            'page' => $page - 1,
            '#url' => Url::fromRoute('ajax_pagination_block.frontpage.data', ['page' => $page - 1]),
            '#attributes' => [
              'class' => [
                'use-ajax',
              ],
            ],
          ];
          // Previous.
          $prevAjaxRes = $items['prev'];
          unset($items['prev']);
        }

        $items[$p] = [
          '#type' => 'link',
          '#title' => $p,
          '#url' => Url::fromRoute('ajax_pagination_block.frontpage.data', ['page' => $p]),
          '#attributes' => [
            'class' => [
              'use-ajax',
            ],
          ],
        ];
      }
      else {
        $items[$p] = [
          '#type' => 'link',
          '#title' => $p,
          '#url' => Url::fromRoute('ajax_pagination_block.frontpage.data', ['page' => $p]),
          '#attributes' => [
            'class' => [
              'use-ajax',
            ],
          ],
        ];
      }

      // Last.
      if ($page != $pages) {
        $items['next'] = [
          '#type' => 'link',
          '#title' => "»",
          'page' => $page + 1,
          '#url' => Url::fromRoute('ajax_pagination_block.frontpage.data', ['page' => $page + 1]),
          '#attributes' => [
            'class' => [
              'use-ajax',
            ],
          ],
        ];

        // Last.
        $nextAjaxRes = $items['next'];
        unset($items['next']);
      }
    }

    $next = [
      '#type' => 'link',
      '#title' => '»',
      '#url' => Url::fromRoute('ajax_pagination_block.frontpage.data', ['page' => $currPage + 1]),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
    ];

    $pager = [
      '#theme' => 'ajax_pagination',
      '#items' => $items,
      '#data' => ['next' => $next, 'prev' => $currPage - 1, 'currentPage' => $currPage],
    ];

    $node_stor = \Drupal::entityManager()->getStorage('node');
    $i = 0;

    $articles = $node_stor->loadMultiple($node_id_article);

    $articleData = [];
    foreach ($articles as $nid) {
      $articleData[$i]['headline'] = $nid->getTitle();
      $i++;
    }

    $build = [
      '#theme' => 'ajax_pagination_block',
      '#leadershippager' => $pager,
      '#data' => $articleData,
      '#attached' => [
        'library' => [
          'ajax_pagination_block/ajax_pagination_block',
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    if (\Drupal::request()->isXmlHttpRequest()) {
      $resp = AjaxResponse::create();
      $prevnext = ['next' => $nextAjaxRes, 'prev' => $prevAjaxRes, 'currentPage' => $currPage];
      $resp->addCommand(new HtmlCommand('#replaced-data', $this->getReplacedData($node_id_article, $items, $prevnext)));
      \Drupal::logger('mahi')->notice('<pre>'.print_r($resp, TRUE).'</pre>');
      return $resp;
    }
    else {
      return $build;
    }
  }

  private function getReplacedData($page, $items, $data) {
    $node_stor = \Drupal::entityManager()->getStorage('node');
    $leadership = $node_stor->loadMultiple($page);
    $output = '';
    foreach ($leadership as $nid) {
      $headline = $nid->getTitle();
      //$image = file_create_url($nid->get('field_image')->entity->uri->value);
      //$link = $nid->get('field_link')->getString();
      $output .= "<article class='thought-article'>
                          <div class='article-image'>
                              <img src=''>
                          </div>
                          <div class='article-content'>
                              <h4 class='article-headline'><p>$headline</p>
                              </h4>
                          </div>
                          <a href='' target='_blank'></a>
                      </article>";
    }
    $output .= "<article class='text-center thought-article-more'>
                          <div class='row'>
                              <div class='col-sm-12' style='text-align: center;'>
                                  <ul class='pageNav pagination pagination_all_updates' id='pages'>";

    if (!empty($data['prev'])) {
      $output .= "<li class='pages_1' id='" . $data['prev']['page'] . "'>
                  <a href='/ajax-pagination/block?page=" . $data['prev']['page'] . "' class='use-ajax'>«</a>
              </li>";
    }

    foreach ($items as $key => $item) {
      if ($item['#title'] == $data['currentPage']) {
        $actClass = 'active';
      }
      else {
        $actClass = '';
      }
      $output .= "<li class='pages_1 " . $actClass . "' id='$key'>
                  <a href='/ajax-pagination/block?page=" . $item['#title'] . "' class='use-ajax'>" . $item['#title'] . "</a>
              </li>";
    }

    if (!empty($data['next'])) {
      $output .= "<li class='pages_1' id='" . $data['next']['page'] . "'>
                  <a href='/ajax-pagination/block?page=" . $data['next']['page'] . "' class='use-ajax'>»</a>
              </li>";
    }

    $output .= "</ul></div></div></article>";
    return $output;
  }


  private function getRequestParam($key, $default = NULL) {
    $request = \Drupal::request();
    if ($request->query->get($key)) {
      return $request->query->get($key);
    }
    return $default;
  }
}