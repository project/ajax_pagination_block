/**
 * @file
 * Defines Javascript behaviors common feature.
 */

(function ($, Drupal, drupalSettings) {

    'use strict';

    /**
     * Behaviors for insights_permission module.
     *
     * @type {Drupal~behavior}
     *
     * @prop {Drupal~behaviorAttach} attach
     *   Attaches ajax_pagination_block behavior for common feature.
     */
    Drupal.behaviors.ajax_pagination_block = {
        attach: function (context) {
            // Module Code.
        }
    };

})(jQuery, Drupal, drupalSettings);
